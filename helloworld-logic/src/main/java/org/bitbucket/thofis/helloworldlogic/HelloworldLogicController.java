package org.bitbucket.thofis.helloworldlogic;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tom on 1/3/16.
 */
@RestController
@CrossOrigin
public class HelloworldLogicController {

    @RequestMapping(path = "/hello/{receiver}")
    public String hello(@PathVariable("receiver") final String receiver) {
        return "Hello " + receiver + "!";
    }

}

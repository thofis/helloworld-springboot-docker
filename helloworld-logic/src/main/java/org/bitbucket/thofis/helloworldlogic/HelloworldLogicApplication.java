package org.bitbucket.thofis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloworldLogicApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldLogicApplication.class, args);
	}
}
